
package tools;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "/*" })
public class AuthorizationFilter implements Filter {

	public AuthorizationFilter() {
	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {

			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

			String reqURI = reqt.getRequestURI();
//			System.out.println(reqURI);
			if (reqURI.indexOf("/salir") >= 0){
				HttpSession session = reqt.getSession();
				session.invalidate();
				resp.sendRedirect(reqt.getContextPath() + "/login");
			}
			if (reqURI.indexOf("/login") >= 0 
					|| (reqURI.indexOf("/faces/") >= 0 && reqURI.indexOf("/faces/listadomaestro.xhtml") < 0 && reqURI.indexOf("/faces/generarorden.xhtml") < 0 ) 
					|| reqURI.indexOf("/webjars/") >= 0 
					|| (ses != null && ses.getAttribute("usernombre") != null)
					|| reqURI.indexOf("/public/") >= 0
					|| reqURI.contains("javax.faces.resource")){
				chain.doFilter(request, response);
			}
			else
				resp.sendRedirect(reqt.getContextPath() + "/login");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void destroy() {

	}
}