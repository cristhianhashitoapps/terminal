package tools;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Herramientas {
	
	public static boolean cadenavacia(String cadena) {
		try {
			return cadena == null || cadena.trim().equals("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;			
	}
	
	public static boolean set(Object object, String fieldName, Object fieldValue) {
	    Class<?> clazz = object.getClass();
	    while (clazz != null) {
	        try {
	            Field field = clazz.getDeclaredField(fieldName);
	            field.setAccessible(true);
	            field.set(object, fieldValue);
	            return true;
	        } catch (NoSuchFieldException e) {
	            clazz = clazz.getSuperclass();
	        } catch (Exception e) {
	            throw new IllegalStateException(e);
	        }
	    }
	    return false;
	}
	
	public static <V> V get(Object object, String fieldName) {
	    Class<?> clazz = object.getClass();
	    while (clazz != null) {
	        try {
	            Field field = clazz.getDeclaredField(fieldName);
	            field.setAccessible(true);
	            return (V) field.get(object);
	        } catch (NoSuchFieldException e) {
	            clazz = clazz.getSuperclass();
	        } catch (Exception e) {
	            throw new IllegalStateException(e);
	        }
	    }
	    return null;
	}
	
	public static String darFechaConFomato(Date fecha, String formato){ 
		try {
			String fechaFormato = "";
			SimpleDateFormat format = new SimpleDateFormat(formato); 
			fechaFormato = format.format(fecha);
			return fechaFormato;
		}
		catch (Exception e) {
			return "";
		}
	}
	
	public static Date sumarDiasCalendario(Date fecha, int dias){
		Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.add(Calendar.DATE, dias); //minus number would decrement the days
        return cal.getTime();
        
	}
}
