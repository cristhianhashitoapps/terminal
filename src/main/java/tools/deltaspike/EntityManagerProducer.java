package tools.deltaspike;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

import org.apache.deltaspike.data.api.EntityManagerResolver;
import org.apache.deltaspike.jpa.api.transaction.TransactionScoped;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

@ApplicationScoped
public class EntityManagerProducer 
{
    @PersistenceUnit
    private EntityManagerFactory emf =  Persistence.createEntityManagerFactory( "org.hibernate.tutorial.jpa" );

    @Produces // you can also make this @RequestScoped  
    @Default
    @RequestScoped
    public EntityManager create()
    {    	
        return emf.createEntityManager();
    }

    public void close(@Disposes EntityManager em)
    {
        if (em.isOpen())
        {        	
            em.close();
        }
    }
    
   

	
}