package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the banner database table.
 * 
 */
@Entity
@Table(name="banner")
@NamedQuery(name="Banner.findAll", query="SELECT b FROM Banner b")
public class Banner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	private Boolean activo;

	private Timestamp cacuda;

	@Column(length=200)
	private String descripcion;

	@Column(name="empresa_contratante", length=200)
	private String empresaContratante;

	@Column(length=200)
	private String ruta;

	//bi-directional many-to-one association to Tipobanner
	@ManyToOne
	@JoinColumn(name="tipobanner_id", nullable=false)
	private Tipobanner tipobanner;

	//bi-directional many-to-one association to Programacionbanner
	@OneToMany(mappedBy="banner")
	private List<Programacionbanner> programacionbanners;

	public Banner() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Timestamp getCacuda() {
		return this.cacuda;
	}

	public void setCacuda(Timestamp cacuda) {
		this.cacuda = cacuda;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEmpresaContratante() {
		return this.empresaContratante;
	}

	public void setEmpresaContratante(String empresaContratante) {
		this.empresaContratante = empresaContratante;
	}

	public String getRuta() {
		return this.ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Tipobanner getTipobanner() {
		return this.tipobanner;
	}

	public void setTipobanner(Tipobanner tipobanner) {
		this.tipobanner = tipobanner;
	}

	public List<Programacionbanner> getProgramacionbanners() {
		return this.programacionbanners;
	}

	public void setProgramacionbanners(List<Programacionbanner> programacionbanners) {
		this.programacionbanners = programacionbanners;
	}

	public Programacionbanner addProgramacionbanner(Programacionbanner programacionbanner) {
		getProgramacionbanners().add(programacionbanner);
		programacionbanner.setBanner(this);

		return programacionbanner;
	}

	public Programacionbanner removeProgramacionbanner(Programacionbanner programacionbanner) {
		getProgramacionbanners().remove(programacionbanner);
		programacionbanner.setBanner(null);

		return programacionbanner;
	}

}