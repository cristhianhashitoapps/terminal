package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the programacionbanner database table.
 * 
 */
@Entity
@Table(name="programacionbanner")
@NamedQuery(name="Programacionbanner.findAll", query="SELECT p FROM Programacionbanner p")
public class Programacionbanner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	//bi-directional many-to-one association to Banner
	@ManyToOne
	@JoinColumn(name="banner_id", nullable=false)
	private Banner banner;

	//bi-directional many-to-one association to Programacion
	@ManyToOne
	@JoinColumn(name="programacion_id", nullable=false)
	private Programacion programacion;

	public Programacionbanner() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Banner getBanner() {
		return this.banner;
	}

	public void setBanner(Banner banner) {
		this.banner = banner;
	}

	public Programacion getProgramacion() {
		return this.programacion;
	}

	public void setProgramacion(Programacion programacion) {
		this.programacion = programacion;
	}

}