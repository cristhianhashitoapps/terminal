package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the programacion database table.
 * 
 */
@Entity
@Table(name="programacion")
@NamedQuery(name="Programacion.findAll", query="SELECT p FROM Programacion p")
public class Programacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	private Timestamp fecha;

	//bi-directional many-to-one association to Programacionbanner
	@OneToMany(mappedBy="programacion")
	private List<Programacionbanner> programacionbanners;

	public Programacion() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public List<Programacionbanner> getProgramacionbanners() {
		return this.programacionbanners;
	}

	public void setProgramacionbanners(List<Programacionbanner> programacionbanners) {
		this.programacionbanners = programacionbanners;
	}

	public Programacionbanner addProgramacionbanner(Programacionbanner programacionbanner) {
		getProgramacionbanners().add(programacionbanner);
		programacionbanner.setProgramacion(this);

		return programacionbanner;
	}

	public Programacionbanner removeProgramacionbanner(Programacionbanner programacionbanner) {
		getProgramacionbanners().remove(programacionbanner);
		programacionbanner.setProgramacion(null);

		return programacionbanner;
	}

}