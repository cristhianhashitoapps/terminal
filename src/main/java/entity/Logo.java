package entity;

import java.io.Serializable;

public class Logo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String nombre;

	public Logo(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
