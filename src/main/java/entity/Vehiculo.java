package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the vehiculo database table.
 * 
 */
@Entity
@Table(name="vehiculo")
@NamedQuery(name="Vehiculo.findAll", query="SELECT v FROM Vehiculo v")
public class Vehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(length=50)
	private String numero;

	@Column(length=50)
	private String placa;

	//bi-directional many-to-one association to Control
	@OneToMany(mappedBy="vehiculo")
	private List<Control> controls;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="empresa_id", nullable=false)
	private Empresa empresa;

	//bi-directional many-to-one association to Linea
	@ManyToOne
	@JoinColumn(name="linea_id", nullable=false)
	private Linea linea;

	//bi-directional many-to-one association to Tipogrupo
	@ManyToOne
	@JoinColumn(name="tipogrupo_id", nullable=false)
	private Tipogrupo tipogrupo;

	//bi-directional many-to-one association to Tiposervicio
	@ManyToOne
	@JoinColumn(name="tiposervicio_id", nullable=false)
	private Tiposervicio tiposervicio;

	public Vehiculo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public List<Control> getControls() {
		return this.controls;
	}

	public void setControls(List<Control> controls) {
		this.controls = controls;
	}

	public Control addControl(Control control) {
		getControls().add(control);
		control.setVehiculo(this);

		return control;
	}

	public Control removeControl(Control control) {
		getControls().remove(control);
		control.setVehiculo(null);

		return control;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Linea getLinea() {
		return this.linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	public Tipogrupo getTipogrupo() {
		return this.tipogrupo;
	}

	public void setTipogrupo(Tipogrupo tipogrupo) {
		this.tipogrupo = tipogrupo;
	}

	public Tiposervicio getTiposervicio() {
		return this.tiposervicio;
	}

	public void setTiposervicio(Tiposervicio tiposervicio) {
		this.tiposervicio = tiposervicio;
	}

}