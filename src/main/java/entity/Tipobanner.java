package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipobanner database table.
 * 
 */
@Entity
@Table(name="tipobanner")
@NamedQuery(name="Tipobanner.findAll", query="SELECT t FROM Tipobanner t")
public class Tipobanner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(length=200)
	private String nombre;

	//bi-directional many-to-one association to Banner
	@OneToMany(mappedBy="tipobanner")
	private List<Banner> banners;

	public Tipobanner() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Banner> getBanners() {
		return this.banners;
	}

	public void setBanners(List<Banner> banners) {
		this.banners = banners;
	}

	public Banner addBanner(Banner banner) {
		getBanners().add(banner);
		banner.setTipobanner(this);

		return banner;
	}

	public Banner removeBanner(Banner banner) {
		getBanners().remove(banner);
		banner.setTipobanner(null);

		return banner;
	}

}