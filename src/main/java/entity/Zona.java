package entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the zona database table.
 * 
 */
@Entity
@Table(name="zona")
@NamedQuery(name="Zona.findAll", query="SELECT z FROM Zona z")
public class Zona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(length=50)
	private String nombre;

	//bi-directional many-to-one association to Control
	@OneToMany(mappedBy="zona")
	private List<Control> controls;

	public Zona() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Control> getControls() {
		return this.controls;
	}

	public void setControls(List<Control> controls) {
		this.controls = controls;
	}

	public Control addControl(Control control) {
		getControls().add(control);
		control.setZona(this);

		return control;
	}

	public Control removeControl(Control control) {
		getControls().remove(control);
		control.setZona(null);

		return control;
	}

}