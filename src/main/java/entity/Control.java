package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;



/**
 * The persistent class for the control database table.
 * 
 */
@Entity
@Table(name="control")
@NamedQuery(name="Control.findAll", query="SELECT c FROM Control c")
public class Control implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(length=200)
	private String destino;

	private Date fecha;

	@Column(name="hora_salida", length=200)
	private String horaSalida;

	@Column(name="responsable_rodamiento", length=200)
	private String responsableRodamiento;

	//bi-directional many-to-one association to Vehiculo
	@ManyToOne
	@JoinColumn(name="vehiculo_id", nullable=false)
	private Vehiculo vehiculo;

	//bi-directional many-to-one association to Zona
	@ManyToOne
	@JoinColumn(name="zona_id", nullable=false)
	private Zona zona;
	
	private Boolean activo;
	
	private Timestamp fechasis;

	public Control() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDestino() {
		return this.destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHoraSalida() {
		return this.horaSalida;
	}

	public void setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
	}

	public String getResponsableRodamiento() {
		return this.responsableRodamiento;
	}

	public void setResponsableRodamiento(String responsableRodamiento) {
		this.responsableRodamiento = responsableRodamiento;
	}

	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Zona getZona() {
		return this.zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Timestamp getFechasis() {
		return fechasis;
	}

	public void setFechasis(Timestamp fechasis) {
		this.fechasis = fechasis;
	}
}