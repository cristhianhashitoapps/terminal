package bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import entity.Logo;
import services.LogoService;
 
@ManagedBean
@ViewScoped
public class CarouselView implements Serializable {
     
	private static final long serialVersionUID = 1L;

	private List<Logo> cars;
     
    private Logo selectedCar;
    
    @Inject
    private LogoService service;
     
    @PostConstruct
    public void init() {
        cars = service.createCars(7);
    }
 
    public List<Logo> getCars() {
        return cars;
    }
 
    public void setService(LogoService service) {
        this.service = service;
    }
 
    public Logo getSelectedCar() {
        return selectedCar;
    }
 
    public void setSelectedCar(Logo selectedCar) {
        this.selectedCar = selectedCar;
    }
}