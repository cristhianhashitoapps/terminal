package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;

import entity.Control;
import entity.Empresa;
import entity.Linea;
import entity.Tipogrupo;
import entity.Tiposervicio;
import entity.Vehiculo;
import services.EmpresaService;
import services.LineaService;
import services.TipoGrupoService;
import services.TipoServicioService;
import services.VehiculoService;


@ViewScoped
@ManagedBean(name = "vehiculoBean")
public class VehiculoBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private VehiculoService vehiculoService;
	@Inject
	private TipoGrupoService tipoGrupoService;
	@Inject
	private EmpresaService empresaService;
	@Inject
	private TipoServicioService tipoServicioService;
	@Inject
	private LineaService lineaService;

	private Vehiculo vehiculo;
	private List<Vehiculo> listVehiculos;
	private boolean eliminado;
	private List<Vehiculo> listVehiculosPorEmpresa;
	
	private Empresa empresa;
	private Empresa empresaCombo;
	private Tipogrupo tipogrupo;
	private Tiposervicio tiposervicio;
	@ManagedProperty("#{linea}")
	private Linea linea;
	
	
	private Long idTipogrupo;
	private Long idEmpresa;
	private Long idTiposervicio;
	private Long idLinea;

	public VehiculoBean() {
		vehiculo = new Vehiculo();
	}
	
	@PostConstruct
	public void iniciar() {
		listVehiculos = vehiculoService.consultarTodos();
	}

	public void agregar() {
		Tipogrupo tipogrupo = tipoGrupoService.consultarUno(idTipogrupo);
		Empresa empresa = empresaService.consultarUno(idEmpresa);
		Tiposervicio tiposervicio = tipoServicioService.consultarUno(idTiposervicio);
		Linea linea = lineaService.consultarUno(idLinea);
		vehiculo.setTipogrupo(tipogrupo);
		vehiculo.setEmpresa(empresa);
		vehiculo.setTiposervicio(tiposervicio);
		vehiculo.setLinea(linea);
		
		Vehiculo almacenarVehiculo = vehiculoService.almacenar(vehiculo);

		if (almacenarVehiculo == null) {
			FacesMessage msg = new FacesMessage("El vehiculo ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		vehiculo = new Vehiculo();
	}

	public void editar() {
		System.out.print("Entro");
		if (vehiculoService.actualizar(vehiculo) != null) {
			FacesMessage msg = new FacesMessage("Vehiculo Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El vehiculo agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		vehiculo = new Vehiculo();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public void onRowEdit(RowEditEvent event) {
		
		Vehiculo resol= (Vehiculo) event.getObject();
		if(vehiculoService.actualizar(resol)!=null)
			{
			FacesMessage msg = new FacesMessage("Vehiculo Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			}
		else
			{
			FacesMessage msg = new FacesMessage("El vehiculo agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			
			}
				
	}

	public boolean eliminar() {
		if (vehiculoService.eliminar(vehiculo) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		vehiculo = new Vehiculo();
		return true;
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public List<Vehiculo> getListVehiculos() {
		listVehiculos = vehiculoService.consultarTodos();
		return listVehiculos;
	}

	public void setListVehiculos(List<Vehiculo> listVehiculos) {
		this.listVehiculos = listVehiculos;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Tipogrupo getTipogrupo() {
		return tipogrupo;
	}

	public void setTipogrupo(Tipogrupo tipogrupo) {
		this.tipogrupo = tipogrupo;
	}

	public Tiposervicio getTiposervicio() {
		return tiposervicio;
	}

	public void setTiposervicio(Tiposervicio tiposervicio) {
		this.tiposervicio = tiposervicio;
	}

	public Linea getLinea() {
		return linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdTipogrupo() {
		return idTipogrupo;
	}

	public void setIdTipogrupo(Long idTipogrupo) {
		this.idTipogrupo = idTipogrupo;
	}

	public Long getIdTiposervicio() {
		return idTiposervicio;
	}

	public void setIdTiposervicio(Long idTiposervicio) {
		this.idTiposervicio = idTiposervicio;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}
	
	public void onCompanyChange() {
		if(empresaCombo !=null && !empresaCombo.equals(""))
			listVehiculosPorEmpresa = vehiculoService.consultarPorEmpresa(empresaCombo);
        else
        	listVehiculosPorEmpresa = null;
    }

	public List<Vehiculo> getListVehiculosPorEmpresa() {
		return listVehiculosPorEmpresa;
	}

	public void setListVehiculosPorEmpresa(List<Vehiculo> listVehiculosPorEmpresa) {
		this.listVehiculosPorEmpresa = listVehiculosPorEmpresa;
	}

	public Empresa getEmpresaCombo() {
		return empresaCombo;
	}

	public void setEmpresaCombo(Empresa empresaCombo) {
		this.empresaCombo = empresaCombo;
	}
	
	
	
	
}
