package bean;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.io.InputStream;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;

import entity.Programacionbanner;
import services.ProgramacionBannerService;;

@ViewScoped
@ManagedBean(name = "programacionBannerBean")
@ApplicationScoped
public class ProgramacionBannerBean extends Base implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ProgramacionBannerService programacionBannerService;

	private Programacionbanner programacionbanner;

	private List<Programacionbanner> listprogramacionesBanner;
	private boolean eliminado;

	public ProgramacionBannerBean() {
		programacionbanner = new Programacionbanner();
	}
	
	@PostConstruct
	public void iniciar() {
		//TODO cargar las listas
	}

	public void agregar() {
		Programacionbanner almacenarBanner = programacionBannerService.almacenar(programacionbanner);

		if (almacenarBanner == null) {
			FacesMessage msg = new FacesMessage("La programacion banner ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			// save programacionbanner

		}
		FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	public void editar() {
		System.out.print("Entro");

		if (programacionBannerService.actualizar(programacionbanner) != null) {
			FacesMessage msg = new FacesMessage("Programacion Editada");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("La programacion agregada ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		programacionbanner = new Programacionbanner();
	}

	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (programacionBannerService.eliminar(programacionbanner) == true) {

			/*
			 * if (filtervendedore != null) { filtervendedore.remove(vendedor);
			 * }
			 */
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		programacionbanner = new Programacionbanner();
		return true;
	}

}
