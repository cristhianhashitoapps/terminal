package bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.primefaces.event.RowEditEvent;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import entity.Control;
import entity.Vehiculo;
import services.ControlService;
import services.VehiculoService;

@ManagedBean(name = "controlBean")
@ViewScoped
public class ControlBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private ControlService controlService;
	
	@Inject VehiculoService vehiculoService;

	private Control control;
	private List<Control> listControles;
	private boolean eliminado;
	private Date fechaDate;
	private Date horaDate;
	private List<Vehiculo> listVehiculosPorEmpresa;

	public ControlBean() {
		control = new Control();
	}
	
	@PostConstruct
	public void init() {
		listControles = controlService.consultarTodosByActivo();
	}
	
	public void prepararNuevoRegistro(){
		control = new Control();
	}

	public void agregar() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		control.setFecha(fechaDate);
		control.setActivo(true);
		control.setFechasis(timestamp);
		Control almacenarEmpresa = controlService.almacenar(control);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("El control ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			notificarPUSH();

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		control = new Control();
	}

	public void editar() {
		System.out.print("Entro");
		//control.setFecha(fechaDate);
		if (controlService.actualizar(control) != null) {
			FacesMessage msg = new FacesMessage("Control Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El control agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		control = new Control();
		fechaDate = null;
		notificarPUSH();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (controlService.eliminar(control) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		control = new Control();
		notificarPUSH();
		return true;
	}

	public Control getControl() {
		return control;
	}

	public void setControl(Control control) {
		this.control = control;
	}

	public List<Control> getListControles() {
		listControles = controlService.consultarTodosByActivo();
		return listControles;
	}

	public void setListControles(List<Control> listControles) {
		this.listControles = listControles;
	}

	public Date getFechaDate() {
		return fechaDate;
	}

	public void setFechaDate(Date fechaDate) {
		this.fechaDate = fechaDate;
	}

	public Date getHoraDate() {
		return horaDate;
	}

	public void setHoraDate(Date horaDate) {
		this.horaDate = horaDate;
	}
	
	public void notificarPUSH() {

        String summary = "Nuevo registro";
        String detail = "Vehiculo listo en plataforma";
        String CHANNEL = "/notify";

        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish(CHANNEL, new FacesMessage(StringEscapeUtils.escapeHtml(summary), StringEscapeUtils.escapeHtml(detail)));
        //eventBus.publish(CHANNEL, new FacesMessage(StringEscapeUtils.escapeHtml3(summary), StringEscapeUtils.escapeHtml3(detail)));
    }
	
	
	public void onCompanyChange2() {
		if(control.getVehiculo().getEmpresa() !=null && !control.getVehiculo().getEmpresa().equals(""))
			listVehiculosPorEmpresa = vehiculoService.consultarPorEmpresa(control.getVehiculo().getEmpresa());
        else
        	listVehiculosPorEmpresa = null;
    }
	
	public void onCompanyChange3(Control control) {
		if(control.getVehiculo().getEmpresa() !=null && !control.getVehiculo().getEmpresa().equals(""))
			listVehiculosPorEmpresa = vehiculoService.consultarPorEmpresa(control.getVehiculo().getEmpresa());
        else
        	listVehiculosPorEmpresa = null;
    }

	public List<Vehiculo> getListVehiculosPorEmpresa() {
		return listVehiculosPorEmpresa;
	}

	public void setListVehiculosPorEmpresa(List<Vehiculo> listVehiculosPorEmpresa) {
		this.listVehiculosPorEmpresa = listVehiculosPorEmpresa;
	}
	
	
	
}
