package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;

import entity.Linea;
import services.LineaService;


@ViewScoped
@ManagedBean(name = "lineaBean")
public class LineaBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private LineaService lineaService;

	private Linea linea;
	private List<Linea> listLineas;
	private boolean eliminado;

	public LineaBean() {
		linea = new Linea();
	}
	
	@PostConstruct
	public void iniciar() {
		listLineas = lineaService.consultarTodos();
	}

	public void agregar() {
		Linea almacenarEmpresa = lineaService.almacenar(linea);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("La linea ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		linea = new Linea();
	}

	public void editar() {
		System.out.print("Entro");
		if (lineaService.actualizar(linea) != null) {
			FacesMessage msg = new FacesMessage("Linea Editada");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("La linea agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		linea = new Linea();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (lineaService.eliminar(linea) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		linea = new Linea();
		return true;
	}

	public Linea getLinea() {
		return linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	public List<Linea> getListLineas() {
		listLineas = lineaService.consultarTodos();
		return listLineas;
	}

	public void setListLineas(List<Linea> listLineas) {
		this.listLineas = listLineas;
	}
	
	
	
}
