package bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;
import org.primefaces.json.JSONArray;

import com.google.gson.Gson;

import entity.Banner;
import services.BannerService;



@ViewScoped
@ManagedBean(name = "bannerBean")
public class BannerBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private BannerService bannerService;

	private Banner banner;
	private List<Banner> listBanners;
	private boolean eliminado;

	public BannerBean() {
		banner = new Banner();
	}
	
	@PostConstruct
	public void iniciar() {
		listBanners = bannerService.consultarTodos();
	}

	public void agregar() {
		Banner almacenarEmpresa = bannerService.almacenar(banner);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("El banner ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		banner = new Banner();
	}

	public void editar() {
		System.out.print("Entro");
		if (bannerService.actualizar(banner) != null) {
			FacesMessage msg = new FacesMessage("Banner Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El banner agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		banner = new Banner();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (bannerService.eliminar(banner) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		banner = new Banner();
		return true;
	}

	public Banner getBanner() {
		return banner;
	}

	public void setBanner(Banner banner) {
		this.banner = banner;
	}

	public List<Banner> getListBanners() {
		listBanners = bannerService.consultarTodos();
		return listBanners;
	}

	public void setListBanners(List<Banner> listBanners) {
		this.listBanners = listBanners;
	}
	
	
	public String getListVideos(){
		
		ArrayList<String> list = bannerService.listActiveVideos();
		//list.add("//localhost:8084/terminal/videos/tinpu_error.mp4");
		//list.add("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
		String json = new Gson().toJson(list);
		
		return json;	
	}
	
	
	
	
	
}
