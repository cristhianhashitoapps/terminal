package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;

import entity.Tiposervicio;
import services.TipoServicioService;


@ViewScoped
@ManagedBean(name = "tipoServicioBean")
public class TipoServicioBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private TipoServicioService tipoServicioService;

	private Tiposervicio tipoServicio;
	private List<Tiposervicio> listTiposServicios;
	private boolean eliminado;

	public TipoServicioBean() {
		tipoServicio = new Tiposervicio();
	}
	
	@PostConstruct
	public void iniciar() {
		listTiposServicios = tipoServicioService.consultarTodos();
	}

	public void agregar() {
		Tiposervicio almacenarEmpresa = tipoServicioService.almacenar(tipoServicio);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("El servicio ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		tipoServicio = new Tiposervicio();
	}

	public void editar() {
		System.out.print("Entro");
		if (tipoServicioService.actualizar(tipoServicio) != null) {
			FacesMessage msg = new FacesMessage("Tipo Servicio Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El tipo de servicio agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		tipoServicio = new Tiposervicio();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (tipoServicioService.eliminar(tipoServicio) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		tipoServicio = new Tiposervicio();
		return true;
	}

	public Tiposervicio getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(Tiposervicio tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public List<Tiposervicio> getListTiposServicios() {
		listTiposServicios = tipoServicioService.consultarTodos();
		return listTiposServicios;
	}

	public void setListTiposServicios(List<Tiposervicio> listTiposServicios) {
		this.listTiposServicios = listTiposServicios;
	}
	
	
}
