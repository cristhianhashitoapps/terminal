package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import entity.Departamento;
import entity.Pais;
import services.DepartamentoService;
import services.PaisService;


@ViewScoped
@ManagedBean(name = "departamentoBean")
public class DepartamentoBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private DepartamentoService departamentoService;
	
	@Inject
	private PaisService paisService;

	private Departamento departamento;
	
	private List<Departamento> listDepartamentos;
	private List<Departamento> listDepartamentosPorPais;
	private Pais paisCombo;

	public DepartamentoBean() {
		departamento = new Departamento();
		
	}
	
	@PostConstruct
	public void iniciar() {
		//listDepartamentos = departamentoService.consultarTodos();
		paisCombo = paisService.consultarUno((long) 47);
		//listDepartamentosPorPais = departamentoService.consultarPorPais(paisCombo);
		onCountryChange();
	}
	

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public List<Departamento> getListDepartamentos() {
		return listDepartamentos;
	}

	public void setListDepartamentos(List<Departamento> listDepartamentos) {
		this.listDepartamentos = listDepartamentos;
	}

	public List<Departamento> getListDepartamentosPorPais() {
		return listDepartamentosPorPais;
	}

	public void setListDepartamentosPorPais(List<Departamento> listDepartamentosPorPais) {
		this.listDepartamentosPorPais = listDepartamentosPorPais;
	}

	public Pais getPaisCombo() {
		return paisCombo;
	}

	public void setPaisCombo(Pais paisCombo) {
		this.paisCombo = paisCombo;
	}
	
	public void onCountryChange() {
		if(paisCombo !=null && !paisCombo.equals(""))
			listDepartamentosPorPais = departamentoService.consultarPorPais(paisCombo);
        else
        	listDepartamentosPorPais = null;
    }
	
}
