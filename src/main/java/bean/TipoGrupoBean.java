package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;

import entity.Tipogrupo;
import services.TipoGrupoService;


@ViewScoped
@ManagedBean(name = "tipoGrupoBean")
public class TipoGrupoBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private TipoGrupoService tipoGrupoService;

	private Tipogrupo tipoGrupo;
	private List<Tipogrupo> listTiposGrupos;
	private boolean eliminado;

	public TipoGrupoBean() {
		tipoGrupo = new Tipogrupo();
	}
	
	@PostConstruct
	public void iniciar() {
		listTiposGrupos = tipoGrupoService.consultarTodos();
	}

	public void agregar() {
		Tipogrupo almacenarEmpresa = tipoGrupoService.almacenar(tipoGrupo);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("El grupo ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		tipoGrupo = new Tipogrupo();
	}

	public void editar() {
		System.out.print("Entro");
		if (tipoGrupoService.actualizar(tipoGrupo) != null) {
			FacesMessage msg = new FacesMessage("Tipo Grupo Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El tipo de grupo agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		tipoGrupo = new Tipogrupo();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (tipoGrupoService.eliminar(tipoGrupo) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		tipoGrupo = new Tipogrupo();
		return true;
	}
	
	public Tipogrupo getTipoGrupo() {
		return tipoGrupo;
	}

	public void setTipoGrupo(Tipogrupo tipoGrupo) {
		this.tipoGrupo = tipoGrupo;
	}

	public List<Tipogrupo> getListTiposGrupos() {
		listTiposGrupos = tipoGrupoService.consultarTodos();
		return listTiposGrupos;
	}

	public void setListTiposGrupos(List<Tipogrupo> listTiposGrupos) {
		this.listTiposGrupos = listTiposGrupos;
	}
}
