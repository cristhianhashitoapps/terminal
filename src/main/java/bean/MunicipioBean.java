package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import entity.Departamento;
import entity.Municipio;
import services.MunicipioService;


@ViewScoped
@ManagedBean(name = "municipioBean")
public class MunicipioBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private MunicipioService municipioService;
	
	private Municipio municipio;
	private Municipio municipioCombo;
	private Departamento departamentoCombo;
	private List<Municipio> listMunicipiosPorDepartamento;

	public MunicipioBean() {
		municipioCombo = new Municipio();
		municipio = new Municipio();
		departamentoCombo = new Departamento();
	}
	
	@PostConstruct
	public void iniciar() {
		//listMunicipiosPorDepartamento = municipioService.consultarPorDepartamento(departamento);
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<Municipio> getListMunicipiosPorDepartamento() {
		return listMunicipiosPorDepartamento;
	}

	public void setListMunicipiosPorDepartamento(List<Municipio> listMunicipiosPorDepartamento) {
		this.listMunicipiosPorDepartamento = listMunicipiosPorDepartamento;
	}

	public Municipio getMunicipioCombo() {
		return municipioCombo;
	}

	public void setMunicipioCombo(Municipio municipioCombo) {
		this.municipioCombo = municipioCombo;
	}

	public Departamento getDepartamentoCombo() {
		return departamentoCombo;
	}

	public void setDepartamentoCombo(Departamento departamentoCombo) {
		this.departamentoCombo = departamentoCombo;
	}
	
	public void onDepartamentoChange() {
		if(departamentoCombo !=null && !departamentoCombo.equals(""))
			listMunicipiosPorDepartamento = municipioService.consultarPorDepartamento(departamentoCombo);
        else
        	listMunicipiosPorDepartamento = null;
    }
}
