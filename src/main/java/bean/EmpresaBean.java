package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.primefaces.event.RowEditEvent;

import entity.Empresa;
import services.EmpresaService;

@ViewScoped
@ManagedBean(name = "empresaBean")
public class EmpresaBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private EmpresaService empresaService;

	private Empresa empresa;
	private List<Empresa> listEmpresas;
	private boolean eliminado;

	public EmpresaBean() {
		empresa = new Empresa();
	}
	
	@PostConstruct
	public void iniciar() {
		listEmpresas = empresaService.consultarTodos();
	}

	public void agregar() {
		Empresa almacenarEmpresa = empresaService.almacenar(empresa);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("La empresa ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		empresa = new Empresa();
	}

	public void editar() {
		System.out.print("Entro");
		if (empresaService.actualizar(empresa) != null) {
			FacesMessage msg = new FacesMessage("Empresa Editada");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("La empresa agregada ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		empresa = new Empresa();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (empresaService.eliminar(empresa) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		empresa = new Empresa();
		return true;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Empresa> getListEmpresas() {
		listEmpresas = empresaService.consultarTodos();
		return listEmpresas;
	}

	public void setListEmpresas(List<Empresa> listEmpresas) {
		this.listEmpresas = listEmpresas;
	}
}
