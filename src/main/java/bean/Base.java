package bean;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.ocpsoft.pretty.PrettyContext;

//import bd.DTO.UsuarioDTO;
//import bd.DTO.UsuariorolDTO;
import tools.SessionUtils;

public class Base implements Serializable {

	private static final long serialVersionUID = 8481307953259648466L;
	
	//protected UsuariorolDTO usuariorol = new UsuariorolDTO();
	//private UsuarioDTO usuario = new UsuarioDTO();
	
	protected void cargarUsuarioRol() {
		try { 
			HttpSession session = SessionUtils.getSession();
			String tempusuario = (String) session.getAttribute("username");
			String tempusuariorol = (String) session.getAttribute("usuariorol");

			//usuariorol = new UsuariorolDTO();
			//usuariorol.setId(tempusuariorol);
			//usuariorol = svc.encontrarUno(usuariorol);
			
			//usuario = new UsuarioDTO();
			//usuario.setId(tempusuario);
			//usuario = svc.encontrarUno(usuario);
		} catch (Exception e) {
			excepcion(e);
		}
	}
	
	protected void advertencia(String titulo, String detalle) {
        FacesContext.getCurrentInstance().addMessage(null, 
        		new FacesMessage(FacesMessage.SEVERITY_WARN, titulo,detalle));
	}
	
	protected void advertencia(String detalle) {
        FacesContext.getCurrentInstance().addMessage(null, 
        		new FacesMessage(FacesMessage.SEVERITY_WARN, "ADVERTENCIA",detalle));
	}
	
	protected void informacion(String titulo, String detalle) {
        FacesContext.getCurrentInstance().addMessage(null, 
        		new FacesMessage(FacesMessage.SEVERITY_INFO, titulo,detalle));
	}
	
	protected void informacion(String detalle) {
        FacesContext.getCurrentInstance().addMessage(null, 
        		new FacesMessage(FacesMessage.SEVERITY_INFO, "CORRECTO",detalle));
	}
	
	protected void excepcion(Exception e) {
		
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		
		///escribir log
		 /*FileWriter fichero = null;
	        PrintWriter pw = null;
	        try
	        {
	            fichero = new FileWriter("/home/logs/log.txt");
	            pw = new PrintWriter(fichero);
	            pw.append(errors.toString());

	        } catch (Exception exx) {
	            exx.printStackTrace();
	        } finally {
	           try {
	           // Nuevamente aprovechamos el finally para 
	           // asegurarnos que se cierra el fichero.
	           if (null != fichero)
	              fichero.close();
	           } catch (Exception e2) {
	              e2.printStackTrace();
	           }
	        }*/
	        ///escribir log
		
		
		String mensaje = "Ocurri� un error, comun�quese con el Administrador.";
        FacesContext.getCurrentInstance().addMessage(null,
        		//new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR",e.toString()));
        		new FacesMessage(FacesMessage.SEVERITY_WARN, "Sistema",mensaje));
        e.printStackTrace();
        
	}
	
	protected void redireccionar(String url) throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect(url);
	}
	
	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "login";
	}
	
	public void validandoAcceso(){
		try {
			HttpSession session = SessionUtils.getSession();
			String tempusuariorol = (String) session.getAttribute("usuariorol");

			//usuariorol = new UsuariorolDTO();
			//usuariorol.setId(tempusuariorol);
			//usuariorol = svc.encontrarUno(usuariorol);
			
			String path = PrettyContext.getCurrentInstance().getRequestURL().toString() ;
			
			//if (!svc.validaAcceso(usuariorol.getRol(),path)){
			//	redireccionar("/calidoso/home");
			//} 
		} catch (Exception e) {
			excepcion(e);
		}
	}

	/*public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public UsuariorolDTO getUsuariorol() {
		return usuariorol;
	}*/

}
