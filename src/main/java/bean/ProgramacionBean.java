package bean;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.transaction.Transactional;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.UploadedFile;

import entity.Banner;
import entity.Programacion;
import entity.Programacionbanner;
import entity.Tipobanner;
import services.BannerService;
import services.ProgramacionBannerService;
import services.ProgramacionService;
import services.TipoBannerService;

@ViewScoped
@ManagedBean(name = "programacionBean")
@ApplicationScoped
public class ProgramacionBean extends Base implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ProgramacionService programacionService;

	@Inject
	private BannerService bannerService;

	@Inject
	private TipoBannerService tipoBannerService;
	
	@Inject
	private ProgramacionBannerService programacionBannerService;

	
	private Banner banner;
	private Programacion programacion;
	private Programacionbanner programacionbanner;
	private int number;

	private Long tipoBanner;
	private UploadedFile file;
	UploadedFile uploaded_image;
	private UploadedFile uploadedFile;

	
	private List<Programacion> listprogramaciones;
	private boolean eliminado;
	private Date fechaDate;
	private Date fechaDate2;

	public ProgramacionBean() {
		programacion = new Programacion();
		banner = new Banner();
		programacionbanner = new Programacionbanner();
	}
	
	@PostConstruct
	public void iniciar() {
		listprogramaciones = programacionService.consultarTodos();
	}

	public void agregar() {

		programacion.setFecha(new java.sql.Timestamp(fechaDate.getTime()));
		// pst.setTimestamp(5, new
		// java.sql.Timestamp(p.getDatahoraInicio().getTime()));
		if (programacionService.existe(programacion) == true) {
			FacesMessage msg = new FacesMessage("La programacion ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			if (bannerService.existe(banner) == true) {
				FacesMessage msg = new FacesMessage("El banner ya existe");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				Programacion almacenarProgramacion = programacionService.almacenar(programacion);

				if (almacenarProgramacion == null) {
					FacesMessage msg = new FacesMessage("La programacion ya existe");
					FacesContext.getCurrentInstance().addMessage(null, msg);
				} else {

					banner.setCacuda(new java.sql.Timestamp(fechaDate2.getTime()));
					banner.setTipobanner(tipoBannerService.consultarUno(tipoBanner));

					Banner almacenarBanner = bannerService.almacenar(banner);

					if (almacenarBanner == null) {
						FacesMessage msg = new FacesMessage("El banner ya existe");
						FacesContext.getCurrentInstance().addMessage(null, msg);
					} else {
						//save programacionbanner
						programacionbanner.setBanner(banner);
						programacionbanner.setProgramacion(programacion);
						programacionBannerService.almacenar(programacionbanner);
					}
					FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
					FacesContext.getCurrentInstance().addMessage(null, msg);

					/*
					 * if(filtervendedore !=null){
					 * filtervendedore.add(vendedor); }
					 */
				}
			}
		}

		banner = new Banner();
		programacion = new Programacion();
		fechaDate = null;
	}

	public void editar() {
		System.out.print("Entro");

		programacion.setFecha(new java.sql.Timestamp(fechaDate.getTime()));
		if (programacionService.actualizar(programacion) != null) {
			FacesMessage msg = new FacesMessage("Programacion Editada");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("La programacion agregada ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		programacion = new Programacion();
		fechaDate = null;
	}

	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (programacionService.eliminar(programacion) == true) {

			/*
			 * if (filtervendedore != null) { filtervendedore.remove(vendedor);
			 * }
			 */
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		programacion = new Programacion();
		return true;
	}

	public Programacion getProgramacion() {
		return programacion;
	}

	public void setProgramacion(Programacion programacion) {
		this.programacion = programacion;
	}

	public List<Programacion> getListprogramaciones() {
		listprogramaciones = programacionService.consultarTodos();
		return listprogramaciones;
	}

	public void setListprogramaciones(List<Programacion> listprogramaciones) {
		this.listprogramaciones = listprogramaciones;
	}

	public Date getFechaDate() {
		return fechaDate;
	}

	public void setFechaDate(Date fechaDate) {
		this.fechaDate = fechaDate;
	}

	public void upload2() {
		if (file.getSize() > 0) {
			FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		} else {
			FacesMessage message = new FacesMessage("Not Succesful", "file is not uploaded");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void upload() {
		if (file != null) {
			FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
			String fileName = "/Volumes/DATOS/java/terminal/src/main/webapp/images" + event.getFile().getFileName();

			File result = new File(fileName);

			FileOutputStream fileOutputStream = new FileOutputStream(result);

			byte[] buffer = new byte[8192];

			int bulk;

			InputStream inputStream = event.getFile().getInputstream();

			while (true) {
				bulk = inputStream.read(buffer);
				if (bulk < 0) {
					break;
				}
				fileOutputStream.write(buffer, 0, bulk);

				fileOutputStream.flush();
			}

			fileOutputStream.close();
			inputStream.close();

			FacesMessage msg = new FacesMessage("Success", event.getFile().getFileName() + " is uploaded");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (IOException e) {
			e.printStackTrace();
			FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "The files were not uploaded");
			FacesContext.getCurrentInstance().addMessage(null, error);
		}
	}

	public Banner getBanner() {
		return banner;
	}

	public void setBanner(Banner banner) {
		this.banner = banner;
	}

	public Date getFechaDate2() {
		return fechaDate2;
	}

	public void setFechaDate2(Date fechaDate2) {
		this.fechaDate2 = fechaDate2;
	}

	public Long getTipoBanner() {
		return tipoBanner;
	}

	public void setTipoBanner(Long tipoBanner) {
		this.tipoBanner = tipoBanner;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public UploadedFile getUploaded_image() {
		return uploaded_image;
	}

	public void setUploaded_image(UploadedFile uploaded_image) {
		this.uploaded_image = uploaded_image;
	}

	String upload_location;

	public String getUpload_location() {
		return upload_location;
	}

	public void setUpload_location(String upload_location) {
		this.upload_location = upload_location;
	}

	public void handleFileUploadkiller(FileUploadEvent event) {
		uploaded_image = event.getFile();
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
				.getContext();
		/*String v_file_ext = uploaded_image.getFileName().split("\\.")[(uploaded_image.getFileName().split("\\.").length)
				- 1];*/
		String app = servletContext.getContextPath();
		String dir = System.getProperty("jboss.home.dir");
		app = app.replace("/", "");
		String realPatch = servletContext.getRealPath("");
		realPatch = realPatch.replace(File.separator + app + ".war", "");
		upload_location = dir + File.separator + "welcome-content" + File.separator + "videos" + File.separator + uploaded_image.getFileName();
		String name = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
		String port = String.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort());
		String url = "//" + name + ":" + port + "/" + "videos" + "/" + uploaded_image.getFileName();
		FileImageOutputStream imageOutput;
		try {
			imageOutput = new FileImageOutputStream(new File(upload_location));
			imageOutput.write(uploaded_image.getContents(), 0, uploaded_image.getContents().length);
			imageOutput.close();
			FacesMessage msg = new FacesMessage("Success", event.getFile().getFileName() + " is uploaded");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			banner.setRuta(url);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, error);
		} catch (IOException e) {
			e.printStackTrace();
			FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, error);
		}
	}
	
	public void fileUpload(FileUploadEvent event) throws IOException {
	    String path = FacesContext.getCurrentInstance().getExternalContext()
	            .getRealPath("/");
	    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
	    String name = fmt.format(new Date())
	            + event.getFile().getFileName().substring(
	                  event.getFile().getFileName().lastIndexOf('.'));
	    File file = new File(path + "/catalogo_imagens/temporario/" + name);

	    InputStream is = event.getFile().getInputstream();
	    OutputStream out = new FileOutputStream(file);
	    byte buf[] = new byte[1024];
	    int len;
	    while ((len = is.read(buf)) > 0)
	        out.write(buf, 0, len);
	    is.close();
	    out.close();
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
	
	public void handleFileUploadlast(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        UploadedFile file = event.getFile();
        try{
            //Log("fileName: " + file.getFileName());
            FileOutputStream fos = new FileOutputStream(new File(file.getFileName()));
            InputStream is = file.getInputstream();
            int BUFFER_SIZE = 8192;
            byte[] buffer = new byte[BUFFER_SIZE];
            int a;
            while(true){
                a = is.read(buffer);
                if(a < 0) break;
                fos.write(buffer, 0, a);
                fos.flush();
            }
            fos.close();
            is.close();
        }catch(IOException e){
        }
    }
	
	
	 
    public int getNumber() {
        return number;
    }
 
    public void increment() {
        number++;
    }

	
	
}
