package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import entity.Zona;
import services.ZonaService;


@ViewScoped
@ManagedBean(name = "zonaBean")
public class ZonaBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private ZonaService zonaService;
	
	private Zona zona;
	private List<Zona> listZonas;

	public ZonaBean() {
		zona = new Zona();
	}
	
	@PostConstruct
	public void iniciar() {
		listZonas = zonaService.consultarTodos();
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public List<Zona> getListZonas() {
		listZonas = zonaService.consultarTodos();
		return listZonas;
	}

	public void setListZonas(List<Zona> listZonas) {
		this.listZonas = listZonas;
	}
}
