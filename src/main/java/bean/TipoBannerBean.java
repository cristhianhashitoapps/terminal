package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.event.RowEditEvent;

import entity.Tipobanner;
import services.TipoBannerService;


@ViewScoped
@ManagedBean(name = "tipoBannerBean")
public class TipoBannerBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private TipoBannerService tipoBannerService;

	private Tipobanner tipoBanner;
	private List<Tipobanner> listTiposBanners;
	private boolean eliminado;

	public TipoBannerBean() {
		tipoBanner = new Tipobanner();
	}
	
	@PostConstruct
	public void iniciar() {
		listTiposBanners = tipoBannerService.consultarTodos();
	}

	public void agregar() {
		Tipobanner almacenarEmpresa = tipoBannerService.almacenar(tipoBanner);

		if (almacenarEmpresa == null) {
			FacesMessage msg = new FacesMessage("El banner ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		tipoBanner = new Tipobanner();
	}

	public void editar() {
		System.out.print("Entro");
		if (tipoBannerService.actualizar(tipoBanner) != null) {
			FacesMessage msg = new FacesMessage("Tipo Banner Editado");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El tipo de banner agregado ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		tipoBanner = new Tipobanner();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (tipoBannerService.eliminar(tipoBanner) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		tipoBanner = new Tipobanner();
		return true;
	}

	public Tipobanner getTipoBanner() {
		return tipoBanner;
	}

	public void setTipoBanner(Tipobanner tipoBanner) {
		this.tipoBanner = tipoBanner;
	}

	public List<Tipobanner> getListTiposBanners() {
		listTiposBanners = tipoBannerService.consultarTodos();
		return listTiposBanners;
	}

	public void setListTiposBanners(List<Tipobanner> listTiposBanners) {
		this.listTiposBanners = listTiposBanners;
	}
	
	
}
