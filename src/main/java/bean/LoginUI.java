package bean;

import java.io.Console;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.deltaspike.core.api.scope.ViewAccessScoped;

import entity.Usuario;
import repositories.UsuarioRepository;
import services.UsuarioService;
//import bd.DTO.UsuarioDTO;
//import bd.DTO.UsuariorolDTO;
import tools.*;

//import servicios.svc;


@ViewScoped
@Transactional
@ManagedBean(name = "login")
public class LoginUI extends Base{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String login = new String();
	private String contrasena = new String();

	//@Inject
	//private UsuarioRepository usuarioRepository;

	@Inject
	private UsuarioService usuarioService;

	public  LoginUI(){
		try {
			login = new String();
			contrasena = new String();
		} catch (Exception e) {
			excepcion(e);
		}
	}

	public void procesarIngreso(){
		try {
			if (Herramientas.cadenavacia(login)){
		        advertencia("El login es necesario.");
		        return;
			}
			if(Herramientas.cadenavacia(contrasena)){
				advertencia("La contraseña es necesaria. ");
		        return;
			}

			//UsuarioService usuarioService = new UsuarioService();
			//usuarioService.consultarUsuario();

			List<Usuario> listas = usuarioService.consultarTodos();


			Usuario usuEncontrado= null;
			for(Usuario usu: listas){
				if(usu.getUsuario().equals(login) && usu.getPass().equals(contrasena)){
					usuEncontrado = usu;
					break;
				}
			}

			if(usuEncontrado!=null){
				login(usuEncontrado);
			}

			//String hola=""+listas.size();

			//Usuario usu = usuarioService.consultarUno((long)9);

			//String nombre = usu.getNombre();

			//Usuario objusu = new Usuario();
			//objusu.setNombre("mariol landa");
			//objusu.setPassword2("123");
			//objusu.setUsuario("mlanda");

			//usuarioService.almacenar(objusu);

			//System.out.println(messageBean.toString()+hola);
			//Usuario usu = new Usuario();
			//usu.setNombre("jaja");
			//usu.setPassword2("apell");
			//usu.setUsuario("nadie");
			//usuarioRepository.saveAndFlush(usu);

			//List<Usuario> lista = usuarioRepository.findAll();
			//String count = lista.size()+"";

			//UsuarioDTO usuario = new UsuarioDTO();
			//usuario.setLogin(login);
			//usuario.setContrasena(contrasena);
			//usuario = svc.encontrarUno(usuario);
			/*if (usuario == null){
				advertencia("La contrase�a o el login esta errado ");
		        return;
			}*/

			//UsuariorolDTO filUsu = new UsuariorolDTO();
			//filUsu.setUsuario(usuario.getId());
			//List<UsuariorolDTO> usuarioRoles = svc.encontrar(filUsu);
			//if (usuarioRoles.size() == 0){
			//	advertencia("No tiene roles asignados");
			//	return;
			//}

			//UsuariorolDTO usuariorol = null;
			//for (UsuariorolDTO temp : usuarioRoles) {
			//	if (temp.getRol().equals("admin")){
			//		usuariorol = temp;
			//	}
			//}
			//if (usuariorol == null){
			//	usuariorol = usuarioRoles.get(0);
			//}
			//informacion("Funciona");
			//login(usuario,usuariorol);
		} catch (Exception e) {
			excepcion(e);
		}
	}



	public void login(Usuario usuario) {
		try {
			HttpSession session = SessionUtils.getSession();
			session.setAttribute("userid", usuario.getId());
			session.setAttribute("usernombre", usuario.getUsuario());

			redireccionar("/terminal/home");
		} catch (Exception e) {
			excepcion(e);
		}
	}


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


}
