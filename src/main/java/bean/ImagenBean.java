package bean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.inject.Inject;
import javax.servlet.ServletContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.UploadedFile;

import entity.Imagen;
import services.ImagenService;


@ViewScoped
@ManagedBean(name = "imagenBean")
public class ImagenBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private ImagenService imagenService;

	private Imagen imagen;
	private List<Imagen> listImagenes;
	private boolean eliminado;
	UploadedFile uploaded_image;
	private UploadedFile uploadedFile;
	private String upload_location;

	public ImagenBean() {
		imagen = new Imagen();
	}
	
	@PostConstruct
	public void iniciar() {
		listImagenes = imagenService.consultarTodos();
	}

	public void agregar() {
		Imagen almacenarImagen = imagenService.almacenar(imagen);

		if (almacenarImagen == null) {
			FacesMessage msg = new FacesMessage("La imagen ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("El registro se agrego exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			/*
			 * if(filtervendedore !=null){ filtervendedore.add(vendedor); }
			 */
		}
		imagen = new Imagen();
	}

	public void editar() {
		System.out.print("Entro");
		if (imagenService.actualizar(imagen) != null) {
			FacesMessage msg = new FacesMessage("Imagen Editada");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage("La imagen agregada ya existe");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		imagen = new Imagen();
	}
	
	public void onRowCancel(RowEditEvent event) {
		FacesMessage msg = new FacesMessage("opcion cancelada");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public boolean eliminar() {
		if (imagenService.eliminar(imagen) == true) {

			/*if (filtervendedore != null) {
				filtervendedore.remove(vendedor);
			}*/
			eliminado = true;
			FacesMessage msg = new FacesMessage("El registro se elimino exitosamente");
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else {
			FacesMessage msg = new FacesMessage("El registro no se pudo eliminar");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			eliminado = false;
		}

		imagen = new Imagen();
		return true;
	}
	
	
	
	public Imagen getImagen() {
		return imagen;
	}

	public void setImagen(Imagen imagen) {
		this.imagen = imagen;
	}

	public List<Imagen> getListImagenes() {
		listImagenes = imagenService.consultarTodos();
		return listImagenes;
	}

	public void setListImagenes(List<Imagen> listImagenes) {
		this.listImagenes = listImagenes;
	}
	
	public void handleFileUploadkiller(FileUploadEvent event) {
		uploaded_image = event.getFile();
		ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
				.getContext();
		/*String v_file_ext = uploaded_image.getFileName().split("\\.")[(uploaded_image.getFileName().split("\\.").length)
				- 1];*/
		String app = servletContext.getContextPath();
		String dir = System.getProperty("jboss.home.dir");
		app = app.replace("/", "");
		String realPatch = servletContext.getRealPath("");
		realPatch = realPatch.replace(File.separator + app + ".war", "");
		upload_location = dir + File.separator + "welcome-content" + File.separator + "logos" + File.separator + uploaded_image.getFileName();
		String name = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
		String port = String.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestServerPort());
		String url = "//" + name + ":" + port + "/" + "logos" + "/" + uploaded_image.getFileName();
		String nameFile = uploaded_image.getFileName();
		FileImageOutputStream imageOutput;
		try {
			imageOutput = new FileImageOutputStream(new File(upload_location));
			imageOutput.write(uploaded_image.getContents(), 0, uploaded_image.getContents().length);
			imageOutput.close();
			FacesMessage msg = new FacesMessage("Success", event.getFile().getFileName() + " is uploaded");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			imagen.setNombre("nombre");
			imagen.setRuta(url);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, error);
		} catch (IOException e) {
			e.printStackTrace();
			FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, error);
		}
	}
	
	public void fileUpload(FileUploadEvent event) throws IOException {
	    String path = FacesContext.getCurrentInstance().getExternalContext()
	            .getRealPath("/");
	    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
	    String name = fmt.format(new Date())
	            + event.getFile().getFileName().substring(
	                  event.getFile().getFileName().lastIndexOf('.'));
	    File file = new File(path + "/catalogo_imagens/temporario/" + name);

	    InputStream is = event.getFile().getInputstream();
	    OutputStream out = new FileOutputStream(file);
	    byte buf[] = new byte[1024];
	    int len;
	    while ((len = is.read(buf)) > 0)
	        out.write(buf, 0, len);
	    is.close();
	    out.close();
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
	
	public void handleFileUploadlast(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        UploadedFile file = event.getFile();
        try{
            //Log("fileName: " + file.getFileName());
            FileOutputStream fos = new FileOutputStream(new File(file.getFileName()));
            InputStream is = file.getInputstream();
            int BUFFER_SIZE = 8192;
            byte[] buffer = new byte[BUFFER_SIZE];
            int a;
            while(true){
                a = is.read(buffer);
                if(a < 0) break;
                fos.write(buffer, 0, a);
                fos.flush();
            }
            fos.close();
            is.close();
        }catch(IOException e){
        }
    }
}
