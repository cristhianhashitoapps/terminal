package bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import entity.Pais;
import services.PaisService;


@ViewScoped
@ManagedBean(name = "paisBean")
public class PaisBean extends Base {

	private static final long serialVersionUID = 1L;

	@Inject
	private PaisService paisService;
	

	private Pais pais;
	private List<Pais> listPaices;
	

	public PaisBean() {
		pais = new Pais();
	}
	
	@PostConstruct
	public void iniciar() {
		listPaices = paisService.consultarTodos();
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public List<Pais> getListPaices() {
		return listPaices;
	}

	public void setListPaices(List<Pais> listPaices) {
		this.listPaices = listPaices;
	}
}
