package services;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import entity.Usuario;
import repositories.UsuarioRepository;
import services.interfaces.IGenericService;
import services.interfaces.IUsuarioService;

@ApplicationScoped
@Default
@Named
public class UsuarioService implements IUsuarioService, IGenericService<Usuario>{
	
	//Las clases Service sirven ademas de acceder a los datos como capa de l�gica de negocio
	
	@Inject
	private UsuarioRepository usuarioRepository;

	@Override
	public Usuario almacenar(Usuario obj) {
		return usuarioRepository.save(obj);
	}

	@Override
	public Long actualizar(Usuario obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Usuario obj) {
		// TODO Auto-generated method stub
		return false;
		
	}

	@Override
	public Usuario consultarUno(Long id) {
		// TODO Auto-generated method stub
		return usuarioRepository.findBy(id);
		
	}

	@Override
	public List<Usuario> consultarTodos() {
		return usuarioRepository.findAll();
		
	}

	@Override
	public void verificarCartera() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean existe(Usuario obj) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
