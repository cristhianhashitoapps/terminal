package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Zona;
import repositories.ZonaRepository;
import services.interfaces.IGenericService;
import services.interfaces.IZonaService;

@Named
public class ZonaService implements IZonaService, IGenericService<Zona> {

	@Inject
	private ZonaRepository zonaRepository;

	@Override
	public Zona almacenar(Zona obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long actualizar(Zona obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Zona obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Zona consultarUno(Long id) {
		return zonaRepository.findBy(id);
	}

	@Override
	public List<Zona> consultarTodos() {
		return zonaRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Zona obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
}
