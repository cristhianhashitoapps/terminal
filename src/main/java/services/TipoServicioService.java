package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Tiposervicio;
import repositories.TipoServicioRepository;
import services.interfaces.IGenericService;
import services.interfaces.ITipoServicioService;

@Named
public class TipoServicioService implements ITipoServicioService, IGenericService<Tiposervicio> {

	@Inject
	private TipoServicioRepository tipoServicioRepository;

	@Override
	public Tiposervicio almacenar(Tiposervicio obj) {
		Tiposervicio encontrado = null;
		try {
			encontrado = tipoServicioRepository.findByNombre(obj.getNombre());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return tipoServicioRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Tiposervicio obj) {
		Tiposervicio encontrado = null;
		try {
			encontrado = tipoServicioRepository.findByNombre(obj.getNombre());
			if (encontrado != null) {
				if (encontrado.getNombre().equals(obj.getNombre())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (tipoServicioRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Tiposervicio obj) {
		try {
			Tiposervicio tipoServicio = tipoServicioRepository.findBy(obj.getId());
			tipoServicioRepository.remove(tipoServicio);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Tiposervicio consultarUno(Long id) {
		return tipoServicioRepository.findBy(id);
	}

	@Override
	public List<Tiposervicio> consultarTodos() {
		return tipoServicioRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Tiposervicio obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
