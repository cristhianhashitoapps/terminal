package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Imagen;
import repositories.ImagenRepository;
import services.interfaces.IGenericService;
import services.interfaces.IImagenService;

@Named
public class ImagenService implements IImagenService, IGenericService<Imagen> {

	@Inject
	private ImagenRepository imagenRepository;

	@Override
	public Imagen almacenar(Imagen obj) {
		Imagen encontrado = null;
		try {
			encontrado = imagenRepository.findByNombre(obj.getNombre());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return imagenRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Imagen obj) {
		Imagen encontrado = null;
		try {
			encontrado = imagenRepository.findByNombre(obj.getNombre());
			if (encontrado != null) {
				if (encontrado.getNombre().equals(obj.getNombre())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (imagenRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Imagen obj) {
		try {
			Imagen imagen = imagenRepository.findBy(obj.getId());
			imagenRepository.remove(imagen);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Imagen consultarUno(Long id) {
		return imagenRepository.findBy(id);
	}

	@Override
	public List<Imagen> consultarTodos() {
		return imagenRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Imagen obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
