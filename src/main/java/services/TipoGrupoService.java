package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Tipogrupo;
import repositories.TipoGrupoRepository;
import services.interfaces.IGenericService;
import services.interfaces.ITipoGrupoService;

@Named
public class TipoGrupoService implements ITipoGrupoService, IGenericService<Tipogrupo> {

	@Inject
	private TipoGrupoRepository tipoGrupoRepository;

	@Override
	public Tipogrupo almacenar(Tipogrupo obj) {
		Tipogrupo encontrado = null;
		try {
			encontrado = tipoGrupoRepository.findByNombre(obj.getNombre());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return tipoGrupoRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Tipogrupo obj) {
		Tipogrupo encontrado = null;
		try {
			encontrado = tipoGrupoRepository.findByNombre(obj.getNombre());
			if (encontrado != null) {
				if (encontrado.getNombre().equals(obj.getNombre())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (tipoGrupoRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Tipogrupo obj) {
		try {
			Tipogrupo tipoGrupo = tipoGrupoRepository.findBy(obj.getId());
			tipoGrupoRepository.remove(tipoGrupo);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Tipogrupo consultarUno(Long id) {
		return tipoGrupoRepository.findBy(id);
	}

	@Override
	public List<Tipogrupo> consultarTodos() {
		return tipoGrupoRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Tipogrupo obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
