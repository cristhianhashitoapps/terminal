package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Tipobanner;
import repositories.TipoBannerRepository;
import services.interfaces.IGenericService;
import services.interfaces.ITipoBannerService;

@Named
public class TipoBannerService implements ITipoBannerService, IGenericService<Tipobanner> {

	@Inject
	private TipoBannerRepository tipoBannerRepository;

	@Override
	public Tipobanner almacenar(Tipobanner obj) {
		Tipobanner encontrado = null;
		try {
			encontrado = tipoBannerRepository.findByNombre(obj.getNombre());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return tipoBannerRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Tipobanner obj) {
		Tipobanner encontrado = null;
		try {
			encontrado = tipoBannerRepository.findByNombre(obj.getNombre());
			if (encontrado != null) {
				if (encontrado.getNombre().equals(obj.getNombre())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (tipoBannerRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Tipobanner obj) {
		try {
			Tipobanner tipoGrupo = tipoBannerRepository.findBy(obj.getId());
			tipoBannerRepository.remove(tipoGrupo);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Tipobanner consultarUno(Long id) {
		return tipoBannerRepository.findBy(id);
	}

	@Override
	public List<Tipobanner> consultarTodos() {
		return tipoBannerRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Tipobanner obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
