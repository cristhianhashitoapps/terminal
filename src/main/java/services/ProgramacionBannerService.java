package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Programacionbanner;
import repositories.ProgramacionBannerRepository;
import services.interfaces.IGenericService;
import services.interfaces.IProgramacionBannerService;

@Named
public class ProgramacionBannerService implements IProgramacionBannerService, IGenericService<Programacionbanner> {

	@Inject
	private ProgramacionBannerRepository programacionBannerRepository;

	@Override
	public Programacionbanner almacenar(Programacionbanner obj) {
		Programacionbanner encontrado = null;
		try {
			encontrado = programacionBannerRepository.findBy(obj.getId());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return programacionBannerRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Programacionbanner obj) {
		Programacionbanner encontrado = null;
		try {
			encontrado = programacionBannerRepository.findBy(obj.getId());
			if (encontrado != null) {
				if (encontrado.getId().equals(obj.getId())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (programacionBannerRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Programacionbanner obj) {
		try {
			Programacionbanner programacionbanner = programacionBannerRepository.findBy(obj.getId());
			programacionBannerRepository.remove(programacionbanner);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Programacionbanner consultarUno(Long id) {
		return programacionBannerRepository.findBy(id);
	}

	@Override
	public List<Programacionbanner> consultarTodos() {
		return programacionBannerRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Programacionbanner obj) {
		Programacionbanner encontrado = null;
		
		try {
			encontrado = programacionBannerRepository.findBy(obj.getId());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}
		if (encontrado != null){
			return true;
		} else {
			return false;
		}
	}

}
