package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Pais;
import repositories.PaisRepository;
import services.interfaces.IGenericService;
import services.interfaces.IPaisService;

@Named
public class PaisService implements IPaisService, IGenericService<Pais> {

	@Inject
	private PaisRepository paisRepository;

	@Override
	public Pais almacenar(Pais obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long actualizar(Pais obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Pais obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Pais consultarUno(Long id) {
		return paisRepository.findBy(id);
	}

	@Override
	public List<Pais> consultarTodos() {
		return paisRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Pais obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
