package services;

import javax.faces.bean.ManagedBean;

import entity.Logo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ApplicationScoped;

@ManagedBean(name = "logoService")
@ApplicationScoped
public class LogoService {
	
	private final static String[] nombres;
	
	static {
		
		nombres = new String[8];
		nombres[0] = "logo_Bolivariano";
		nombres[1] = "logo_cootranar";
		nombres[2] = "logo_cootransmayo";
		nombres[3] = "logo_Exp_valleatriz";
		nombres[4] = "logo_Flota-Magdalena";
		nombres[5] = "logo_supertaxis";
		nombres[6] = "logo_transipiales";
	}
	
	public List<Logo> createCars(int size) {
        List<Logo> list = new ArrayList<Logo>();
		for(int i = 0 ; i < size ; i++) {
			list.add(new Logo(nombres[i]));
        }
        
        return list;
    }

	public List<String> getNombres() {
		return Arrays.asList(nombres);
	}
	
	
	
}
