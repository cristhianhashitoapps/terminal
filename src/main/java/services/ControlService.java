package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Control;
import entity.Vehiculo;
import repositories.ControlRepository;
import services.interfaces.IControlService;
import services.interfaces.IGenericService;

@Named
public class ControlService implements IControlService, IGenericService<Control> {

	@Inject
	private ControlRepository controlRepository;

	@Override
	public Control almacenar(Control obj) {
		Control encontrado = null;
		try {
			//encontrado = controlRepository.findByIdVehiculo_idDestinoFecha(obj.getId(), obj.getVehiculo().getId(), obj.getDestino(), obj.getFecha());
			encontrado = controlRepository.findByVehiculo_idAndFecha(obj.getVehiculo().getId(), obj.getFecha());
			System.out.println("Encontrado " + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado " + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return controlRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Control obj) {
		Control encontrado = null;
		try {
			encontrado = controlRepository.findById(obj.getId());
			if (encontrado != null) {
				if (encontrado.getId().equals(obj.getId())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (controlRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Control obj) {
		try {
			Control empresa = controlRepository.findBy(obj.getId());
			controlRepository.remove(empresa);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Control consultarUno(Long id) {
		return controlRepository.findBy(id);
	}

	@Override
	public List<Control> consultarTodos() {
		return controlRepository.findAllOrderByIdDesc();
	}

	@Override
	public List<Vehiculo> listAllVehiculos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existe(Control obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Control> consultarTodosByActivo() {
		return controlRepository.findAllOrderByActivo();
	}
	

}
