package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Linea;
import repositories.LineaRepository;
import services.interfaces.IGenericService;
import services.interfaces.ILineaService;

@Named
public class LineaService implements ILineaService, IGenericService<Linea> {

	@Inject
	private LineaRepository lineaRepository;

	@Override
	public Linea almacenar(Linea obj) {
		Linea encontrado = null;
		try {
			encontrado = lineaRepository.findByNombre(obj.getNombre());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return lineaRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Linea obj) {
		Linea encontrado = null;
		try {
			encontrado = lineaRepository.findByNombre(obj.getNombre());
			if (encontrado != null) {
				if (encontrado.getNombre().equals(obj.getNombre())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (lineaRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Linea obj) {
		try {
			Linea tipoGrupo = lineaRepository.findBy(obj.getId());
			lineaRepository.remove(tipoGrupo);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Linea consultarUno(Long id) {
		return lineaRepository.findBy(id);
	}

	@Override
	public List<Linea> consultarTodos() {
		return lineaRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Linea obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
