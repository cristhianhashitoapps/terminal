package services.interfaces;

import java.util.List;

import entity.Departamento;
import entity.Municipio;

public interface IMunicipioService {
	public List<Municipio> consultarPorDepartamento(Departamento departamento);
}
