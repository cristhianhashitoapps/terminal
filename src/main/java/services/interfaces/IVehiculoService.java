package services.interfaces;

import java.util.List;

import entity.Empresa;
import entity.Tipogrupo;
import entity.Tiposervicio;
import entity.Vehiculo;

public interface IVehiculoService {
	public List<Tipogrupo>listAllTipoGrupos();
	public List<Empresa>listAllEmpresas();
	public List<Tiposervicio>listAllTipoServicios();
	public List<Vehiculo> consultarPorEmpresa(Empresa empresa);
}
