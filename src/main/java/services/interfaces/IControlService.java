package services.interfaces;

import java.util.List;

import entity.Control;
import entity.Vehiculo;

public interface IControlService {
	public List<Vehiculo>listAllVehiculos();
	public List<Control> consultarTodosByActivo();
}
