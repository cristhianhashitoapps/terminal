package services.interfaces;

import java.util.List;

/***
 * Generic del DAO para acceder a los datos
 * @author Asus
 *
 */
public interface IGenericService<T> {
	public T almacenar(T obj);
	public Long actualizar(T obj);
	public boolean eliminar(T obj);
	public T consultarUno(Long id);
	public List<T> consultarTodos();
	public boolean existe(T obj);
}
