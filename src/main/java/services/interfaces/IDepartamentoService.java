package services.interfaces;

import java.util.List;

import entity.Departamento;
import entity.Pais;

public interface IDepartamentoService {
	public List<Departamento> consultarPorPais(Pais pais);
}
