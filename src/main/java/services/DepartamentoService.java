package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Departamento;
import entity.Pais;
import repositories.DepartamentoRepository;
import services.interfaces.IDepartamentoService;
import services.interfaces.IGenericService;

@Named
public class DepartamentoService implements IDepartamentoService, IGenericService<Departamento> {

	@Inject
	private DepartamentoRepository departamentoRepository;

	@Override
	public Departamento almacenar(Departamento obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long actualizar(Departamento obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Departamento obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Departamento consultarUno(Long id) {
		// TODO Auto-generated method stub
		return departamentoRepository.findBy(id);
	}

	@Override
	public List<Departamento> consultarTodos() {
		return departamentoRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Departamento obj) {
		return false;
	}

	@Override
	public List<Departamento> consultarPorPais(Pais pais) {
		return departamentoRepository.findBypais_id(pais.getId());
	}
}
