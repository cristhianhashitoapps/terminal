package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Empresa;
import repositories.EmpresaRepository;
import services.interfaces.IEmpresaService;
import services.interfaces.IGenericService;

@Named
public class EmpresaService implements IEmpresaService, IGenericService<Empresa> {

	@Inject
	private EmpresaRepository empresaRepository;

	@Override
	public Empresa almacenar(Empresa obj) {
		Empresa encontrado = null;
		try {
			encontrado = empresaRepository.findByNit(obj.getNit());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return empresaRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Empresa obj) {
		Empresa encontrado = null;
		try {
			encontrado = empresaRepository.findByNit(obj.getNit());
			if (encontrado != null) {
				if (encontrado.getNit().equals(obj.getNit())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (empresaRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Empresa obj) {
		try {
			Empresa empresa = empresaRepository.findBy(obj.getId());
			empresaRepository.remove(empresa);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Empresa consultarUno(Long id) {
		return empresaRepository.findBy(id);
	}

	@Override
	public List<Empresa> consultarTodos() {
		return empresaRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Empresa obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
