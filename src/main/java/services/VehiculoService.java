package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Empresa;
import entity.Tipogrupo;
import entity.Tiposervicio;
import entity.Vehiculo;
import repositories.VehiculoRepository;
import services.interfaces.IGenericService;
import services.interfaces.IVehiculoService;

@Named
public class VehiculoService implements IVehiculoService, IGenericService<Vehiculo> {

	@Inject
	private VehiculoRepository vehiculoRepository;

	@Override
	public Vehiculo almacenar(Vehiculo obj) {
		Vehiculo encontrado = null;
		try {
			encontrado = vehiculoRepository.findByPlaca(obj.getPlaca());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return vehiculoRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Vehiculo obj) {
		Vehiculo encontrado = null;
		try {
			encontrado = vehiculoRepository.findByPlaca(obj.getPlaca());
			if (encontrado != null) {
				if (encontrado.getPlaca().equals(obj.getPlaca())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (vehiculoRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Vehiculo obj) {
		try {
			Vehiculo empresa = vehiculoRepository.findBy(obj.getId());
			vehiculoRepository.remove(empresa);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Vehiculo consultarUno(Long id) {
		return vehiculoRepository.findBy(id);
	}

	@Override
	public List<Vehiculo> consultarTodos() {
		return vehiculoRepository.findAllOrderByIdDesc();
	}

	@Override
	public List<Tipogrupo> listAllTipoGrupos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Empresa> listAllEmpresas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Tiposervicio> listAllTipoServicios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existe(Vehiculo obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Vehiculo> consultarPorEmpresa(Empresa empresa) {
		return vehiculoRepository.findByempresa_id(empresa.getId());
	}

}
