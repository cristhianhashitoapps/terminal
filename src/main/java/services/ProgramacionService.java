package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Programacion;
import repositories.ProgramacionRepository;
import services.interfaces.IGenericService;
import services.interfaces.IProgramacionService;

@Named
public class ProgramacionService implements IProgramacionService, IGenericService<Programacion> {

	@Inject
	private ProgramacionRepository programacionRepository;

	@Override
	public Programacion almacenar(Programacion obj) {
		Programacion encontrado = null;
		try {
			encontrado = programacionRepository.findByFecha(obj.getFecha());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return programacionRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Programacion obj) {
		Programacion encontrado = null;
		try {
			encontrado = programacionRepository.findByFecha(obj.getFecha());
			if (encontrado != null) {
				if (encontrado.getFecha().equals(obj.getFecha())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (programacionRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Programacion obj) {
		try {
			Programacion programacion = programacionRepository.findBy(obj.getId());
			programacionRepository.remove(programacion);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Programacion consultarUno(Long id) {
		return programacionRepository.findBy(id);
	}

	@Override
	public List<Programacion> consultarTodos() {
		return programacionRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Programacion obj) {
		Programacion encontrado = null;
		
		try {
			encontrado = programacionRepository.findByFecha(obj.getFecha());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}
		if (encontrado != null){
			return true;
		} else {
			return false;
		}
	}

}
