package services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Departamento;
import entity.Municipio;
import repositories.MunicipioRepository;
import services.interfaces.IGenericService;
import services.interfaces.IMunicipioService;

@Named
public class MunicipioService implements IMunicipioService, IGenericService<Municipio> {

	@Inject
	private MunicipioRepository municipioRepository;

	@Override
	public Municipio almacenar(Municipio obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long actualizar(Municipio obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Municipio obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Municipio consultarUno(Long id) {
		return municipioRepository.findBy(id);
	}

	@Override
	public List<Municipio> consultarTodos() {
		return municipioRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Municipio obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Municipio> consultarPorDepartamento(Departamento departamento) {
		return municipioRepository.findBydepartamento_id(departamento.getId());
	}
	
	
}
