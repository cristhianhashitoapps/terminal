package services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import entity.Banner;
import repositories.BannerRepository;
import services.interfaces.IBannerService;
import services.interfaces.IGenericService;

@Named
public class BannerService implements IBannerService, IGenericService<Banner> {

	@Inject
	private BannerRepository bannerRepository;

	@Override
	public Banner almacenar(Banner obj) {
		Banner encontrado = null;
		try {
			encontrado = bannerRepository.findByDescripcion(obj.getDescripcion());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			return bannerRepository.save(obj);
		}
	}

	@Override
	public Long actualizar(Banner obj) {
		Banner encontrado = null;
		try {
			encontrado = bannerRepository.findByDescripcion(obj.getDescripcion());
			if (encontrado != null) {
				if (encontrado.getDescripcion().equals(obj.getDescripcion())) {
					encontrado = null;
					System.out.print("Es null");
				}
			}

		} catch (Exception e) {
			System.out.print("No encontrado" + e);
		}

		if (encontrado != null) {
			return null;
		} else {
			if (bannerRepository.saveAndFlush(obj) != null) {
				System.out.print("se actualizo");
				return obj.getId();
			} else {
				System.out.print("Return null");
				return null;
			}
		}
	}

	@Override
	public boolean eliminar(Banner obj) {
		try {
			Banner tipoGrupo = bannerRepository.findBy(obj.getId());
			bannerRepository.remove(tipoGrupo);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public Banner consultarUno(Long id) {
		return bannerRepository.findBy(id);
	}

	@Override
	public List<Banner> consultarTodos() {
		return bannerRepository.findAllOrderByIdDesc();
	}

	@Override
	public boolean existe(Banner obj) {
		Banner encontrado = null;

		try {
			encontrado = bannerRepository.findByDescripcion(obj.getDescripcion());
			System.out.println("Encontrado" + encontrado);
		} catch (Exception e) {
			System.out.println("No encontrado" + e);
		}
		if (encontrado != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ArrayList<String> listActiveVideos() {

		ArrayList<String> list = new ArrayList<String>();

		List<Banner> listBanners = bannerRepository.findAllOrderByIdDesc();

		Iterator<Banner> it = listBanners.iterator();

		while (it.hasNext()) {
			Banner myBanner = it.next();
			boolean activo = myBanner.getActivo();

			if (activo) {
				String urlVideo = myBanner.getRuta(); 
				list.add(urlVideo);
			}

		}

		// list.add("//localhost:8084/terminal/videos/tinpu_error.mp4");
		// list.add("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
		return list;
	}

}
