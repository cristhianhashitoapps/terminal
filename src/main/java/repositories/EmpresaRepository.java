package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Empresa;

@Repository
public interface EmpresaRepository extends EntityRepository<Empresa, Long> {
	Empresa findByNit(String nit);
	List<Empresa> findAllOrderByIdDesc();
}
