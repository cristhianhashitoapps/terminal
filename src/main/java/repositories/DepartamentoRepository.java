package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Departamento;


@Repository
public interface DepartamentoRepository extends EntityRepository<Departamento, Long> {
	Departamento findByNombre(String nombre);
	List<Departamento> findAllOrderByIdDesc();
	List<Departamento> findBypais_id(Long paisesId);
}
