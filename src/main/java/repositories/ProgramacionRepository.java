package repositories;

import java.sql.Timestamp;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Programacion;

@Repository
public interface ProgramacionRepository extends EntityRepository<Programacion, Long> {
	Programacion findByFecha(Timestamp fecha);
	List<Programacion> findAllOrderByIdDesc();
}
