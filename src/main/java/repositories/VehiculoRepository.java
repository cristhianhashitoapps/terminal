package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Vehiculo;


@Repository
public interface VehiculoRepository extends EntityRepository<Vehiculo, Long> {
	Vehiculo findByPlaca(String placa);
	List<Vehiculo> findAllOrderByIdDesc();
	List<Vehiculo> findByempresa_id(Long empresaId);
}
