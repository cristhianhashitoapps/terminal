package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Banner;


@Repository
public interface BannerRepository extends EntityRepository<Banner, Long> {
	Banner findByDescripcion(String nombre);
	List<Banner> findAllOrderByIdDesc();
}
