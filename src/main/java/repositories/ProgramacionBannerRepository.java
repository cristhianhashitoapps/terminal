package repositories;

import java.sql.Timestamp;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Programacionbanner;

@Repository
public interface ProgramacionBannerRepository extends EntityRepository<Programacionbanner, Long> {
	List<Programacionbanner> findAllOrderByIdDesc();
}
