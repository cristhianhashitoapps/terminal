package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Imagen;


@Repository
public interface ImagenRepository extends EntityRepository<Imagen, Long> {
	Imagen findByNombre(String nombre);
	List<Imagen> findAllOrderByIdDesc();
}
