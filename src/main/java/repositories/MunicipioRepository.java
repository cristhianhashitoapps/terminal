package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Municipio;


@Repository
public interface MunicipioRepository extends EntityRepository<Municipio, Long> {
	Municipio findByNombre(String nombre);
	List<Municipio> findAllOrderByIdDesc();
	List<Municipio> findBydepartamento_id(Long departamentoId);
}
