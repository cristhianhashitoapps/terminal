package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Linea;


@Repository
public interface LineaRepository extends EntityRepository<Linea, Long> {
	Linea findByNombre(String nombre);
	List<Linea> findAllOrderByIdDesc();
}
