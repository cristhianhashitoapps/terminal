package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Pais;


@Repository
public interface PaisRepository extends EntityRepository<Pais, Long> {
	Pais findByNombre(String nombre);
	List<Pais> findAllOrderByIdDesc();
}
