package repositories;

import java.util.Date;
import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.SingleResultType;

import entity.Control;




@Repository
public interface ControlRepository extends EntityRepository<Control, Long> {
	Control findByIdVehiculo_idDestinoFecha(Long id, Long idVehiculo, String destino, Date fecha);
	Control findById(Long id);
	List<Control> findAllOrderByIdDesc();
	
	@Query(value = "SELECT * FROM control c WHERE c.vehiculo_id = ?1 and c.fecha = ?2", singleResult = SingleResultType.ANY, isNative = true)
	Control findByVehiculo_idAndFecha(Long vehiculo_id, Date fecha);
	
	@Query(value = "SELECT * FROM control c WHERE c.activo = TRUE order by c.id desc", isNative = true)
	List<Control> findAllOrderByActivo();
}

