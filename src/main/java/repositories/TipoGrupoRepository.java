package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Tipogrupo;

@Repository
public interface TipoGrupoRepository extends EntityRepository<Tipogrupo, Long> {
	Tipogrupo findByNombre(String nombre);
	List<Tipogrupo> findAllOrderByIdDesc();
}
