package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Tipobanner;

@Repository
public interface TipoBannerRepository extends EntityRepository<Tipobanner, Long> {
	Tipobanner findByNombre(String nombre);
	List<Tipobanner> findAllOrderByIdDesc();
}
