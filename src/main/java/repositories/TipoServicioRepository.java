package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Tiposervicio;

@Repository
public interface TipoServicioRepository extends EntityRepository<Tiposervicio, Long> {
	Tiposervicio findByNombre(String nombre);
	List<Tiposervicio> findAllOrderByIdDesc();
}
