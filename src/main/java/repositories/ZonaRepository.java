package repositories;

import java.util.List;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import entity.Zona;

@Repository
public interface ZonaRepository extends EntityRepository<Zona, Long> {
	Zona findByNombre(String nombre);
	List<Zona> findAllOrderByIdDesc();
}
